## DB-Modellierung Server (Übungsaufgabe/Klassenarbeit)

<span class="hidden-text" title="mastodonurl">https://bildung.social/@oerinformatik/110431604962244789</span>

<span class="hidden-text" title="arcticleurl">https://oer-informatik.de/db-modellierung-server</span>

> _(50 min Bearbeitungszeit): Erstellen Sie im zugelosten Team (3 Personen) schrittweise ein physisches Datenmodell und dokumentieren Sie die Phasen im Klassenarbeits-git-Repository ihrer Gruppe._

Das Rechenzentrum ihrer Firma benötigt eine Datenbank zum Monitoring der Server- und Applikationsdaten. In der Datenbank sollen Daten über den Ressourcenbedarf der Hardware und Software (Rechenressourcen, Speicher, Energie) sowie die relevanten Rahmendaten der Server hinterlegt werden können.

Die Daten stammen aus einem noch zu entwickelnden Tool (anderes Projekt) und diese Daten sollen später von einem Programm zur Anzeige aufbereitet werden (noch ein anderes Projekt). Hier geht es also nur um den Entwurf der Datenbank.

### Rahmenbedingungen

* Modellieren Sie in gelosten Dreiergruppen die Struktur der geforderten Datenbank in einem konzeptuellen, einem logischen und einem physischen Datenmodell.

* Dokumentieren Sie diesen Entwurf in der Readme.MD-Datei des git-Repository ihrer Gruppe. Nutzen Sie fachgerechte Darstellungsformen der einzelnen Modelle und stellen Sie alle bekannten Eigenschaften der Datenmodelle dar.

* Commits bis zum Ende der Bearbeitungszeit gehen in die Bewertung ein.

* Geben Sie zum Ende der Bearbeitungszeit über den Abgabelink die Bewertungsmatrix für die Teamarbeit ab.

* Die Internetnutzung und alle anderen Medien sind erlaubt, Kommunikation darf jedoch nur innerhalb ihres Teams erfolgen.

### Bewertungsmatrix

|Kriterium|erreichbare Punkte|erreichte Punkte|
|---|---|---|
|**Entity Relationship Modell**<br/>Kurze einführende Beschreibung der Planungsphase<br/>Vollständigkeit des Modells<br/>Fachliche Richtigkeit der Notation und Modellierung<br/>Abgabe als ERD, eingebunden in `Readme.MD` des Repos|25%||
|**Relationenmodell**<br/>Kurze einführende Beschreibung der Planungsphase<br/>Vollständige Transformation aller Infos aus dem ERD<br/>Fachlich richtige Darstellung<br/>Nachweis der Normalform für jedes Relationenschema<br/>Abgabe als RM (Textform oder Diagramm), eingebunden in `Readme.MD` des Repos|25%||
|**Physisches Modell**<br/>Kurze einführende Beschreibung der Planungsphase<br/>Nennung des genutzten DBMS (sonst wird MariaDB angenommen)<br/>Kurze Begründung der Wahl der Datentypen<br/>Korrekte Notation gemäß DDL des jeweiligen DBMS<br/>Abgabe als SQL-DDL-Befehle, eingebunden in `Readme.MD` des Repos|25%||
|**Gruppenphase** (Kriterien s.u., Abgabe auf Papier)||
|Bewertung Teamkompetenzen durch Gruppenmitglieder (siehe unten)|8,6%||
|Selbstbewertung (siehe unten)|8,6%||
|Bewertung durch Lehrer|8,7%||
|**Ergebnis**|100%||

Darüber hinaus gibt es noch einen _procored_ Wissenstest, der über Moodle im Vorfeld bearbeitet wird. Der Moodletest wird mit 40% gewichtet, die Gruppenarbeit mit 60%.

### Konzeptuelles Datenmodell (25%):

Aus einem ersten Gespräch mit der Fachdomäne erhalten Sie folgende Liste der Anforderungen:

- Es gibt physische und virtualisierte Server, beide sollen gemeinsam verwaltet werden. Für jeden Server muss zwingend erfasst werden, ob es sich um einen physischen oder um einen virtualisierten Server handelt.

- Für physische Server soll der Standort erfasst werden. Dazu gehört eine Firmenadresse, die Raumnummer (numerisch, dreistellig), die Rack Nummer (also das "Regal", in dem der Server steht, alphanumerisch, dreistellig). An einer Firmenadresse befinden sich i.d.R. viele physische Server.

- Jeder virtualisierte Server ist einem physischen Server zugeordnet. Es soll zwei Textfelder für virualisierte Server geben: Programmname der Virtualisierungsumgebung ("Hypervisor") und Programmversion (8 Zeichen)

- Die Hardwareinfos beider Serverarten sind CPU-Bezeichnung, Anzahl der Kerne, Tatfrequenz der CPU in GHz und RAM in Gigabyte (z.B. `Intel(R) Core(TM) i5-7300U CPU`, `2`, `2712`, `512`). Diese Infos werden einmal eingegeben, es bedarf keiner Historie. Auch für virtuelle Server werdend die Werte gesondert erfasst.

- Der aktuelle Speicherbedarf wird für jeden physischen oder virtuellen Server stündlich mit Zeitstempel gespeichert. Es soll alles erfasst werden, was über den Befehl `free -h -w` ausgegeben wird:

```bash
$ free -h -w
               total        used        free      shared     buffers       cache   available
Mem:           7.9Gi       7.3Gi       412Mi        17Mi        33Mi       190Mi       505Mi
Swap:           24Gi       5.6Gi        18Gi
```

Darin finden sich Daten zum physischen Speicher (Zeile "Mem") und zum virtuellen Speicher (Zeile "Swap") und zwar jeweils den verfügbaren Speicher (total), den benutzen Speicher (used), den freien Speicher (free), Speicher für das temporäre Dateisystem (shared), Speicher für Kernelpuffer (buffers), Speicherbedarf für den Seitenspeicher (cache), geschätzter verfügbarer Speicher für neue Applikationen (available). Gespeichert werden alle Daten in Byte.

- Weiterhin sollen auf jedem physischen oder virtuellen Server alle 5 Minuten die Prozesse überwacht werden. Hierzu dient die Ausgabe von `ps -eo pid,ppid,user,pcpu,pmem,comm`:

```bash
$ ps -eo pid,ppid,user,pcpu,pmem,comm
  PID  PPID USER     %CPU %MEM COMMAND
    1     0 root      0.0  0.0 init
    8     1 root      0.0  0.0 init
    9     8 adminis+  0.0  0.0 bash
  106     1 root      0.0  0.0 init
  107   106 adminis+  0.0  0.0 bash
  150   107 adminis+  0.0  0.0 pico
  301   107 adminis+  0.0  0.0 man
  311   301 adminis+  0.0  0.0 pager
  331     9 adminis+  0.0  0.0 ps
```

Es soll gespiechert werden
- ein Zeitstempel
- die  Prozessnummer,
- die Nummer der aufrufenden Prozesses (Beispiel: im Terminal mit der Prozess-ID `pid=107` hatte ich den editor `pico` aufgerufen, der dann die Parent Prozess ID `ppid=107` zugewiesen bekommen hat.)
- den Benutzer, der diesen Prozess gestartet hat
- die Prozessorauslastung
- die Speicherauslastung
- den Befehlsnamen



Erstellen Sie ein Entity-Relationship-Modell. Führen Sie hierzu nur die Planungsschritte durch, die in der Phase der konzeptuellen Datenmodellierung fachgerecht sind. Fügen Sie ein Entity-Relationship-Diagramm (in Chen-Notation) in ihr git-Repository ein (toolunterstützt oder als Stiftzeichnung als Foto/Screenshot). Kennzeichnen Sie Kardinalitäten (inkl. Optionalität) sowie optionale, mehrwertige oder abgeleitete Attribute, Relationstypen und optionale Relationen.

### Logisches Datenmodell (25%):

Das obige Datenmodell soll in ein Relationenmodell überführt werden. Führen Sie alle Transformationsschritte durch, die hierzu in der logischen Datenbankmodellierung erforderlich sind. Benennen Sie - soweit erforderlich - auch die Constraints, die sich aus den konzeptuellen Modell ergeben. Achten Sie darauf, dass im Relationenmodell mindestens der gleiche Informationsgehalt existiert, wie im ER-Modell.

Weisen Sie nach, dass ihr Relationenmodell der 3. Normalform genügt bzw. begründen Sie, an welchen Stellen Sie sich bewusst dagegen entschieden haben.

### Physisches Datenmodell (25%):

Entscheiden Sie, welches DBMS Ihnen als Grundlage dienen soll. Dauert die Entscheidung länger als zwei Minuten ist die Grundlage ihrer Lösung MariaDB. Erstellen Sie für das logische Modell die SQL-DDL-Befehle.

Fügen Sie für alle Attribute bei Datentypen und Constraints, bei denen Sie Entscheidungen getroffen haben, kurz als Kommentar einen Stichpunkt als Begründung ein.

```sql
CREATE TABLE beispiel (
    attributname1 DATENTYP CONSTRAINT /*Kurzbegründung f. Constraint od. Datentyp*/,
    attributname2 DATENTYP CONSTRAINT /*Kurzbegründung f. Constraint od. Datentyp*/,
    ...);
```

### Bewertungskriterien für die Gruppenphase (ca. 3 min, Bearbeitung nach Abgabe der Repos)

Die Bewertung der Gruppenprozesse ist essenzieller Bestandteil dieser Gruppen-Klassenarbeit. Diese Bewertung erfolgt aus drei Perspektiven: Jede*r bewertet sich selbst (1. Spalte) und seine beiden Gruppenmitglieder (2. und 3. Spalte). Außerdem erfolgt durch den Lehrer eine Bewertung auf Basis der Beobachtungen während der Bearbeitung.

`++` = trifft voll zu (100%) / `+` = trifft eher zu (75%) / `-` trifft eher nicht zu (50%) / `--` = trifft nicht zu (0%)

|Kriterium|selbst|Teil-<br/>nehmer*in<br/> 1|Teil-<br/>nehmer*in<br/> 2|
|---|---|---|---|
|Sorgt für eine konzentrierte und zielgerichtete Arbeitsatmosphäre im Team.|||||
Arbeitet kooperativ mit den anderen Teammitgliedern: ergänzt, bestärkt, ohne Arbeit an sich zu reißen oder andere zu übergehen.|||||
|Übernimmt Aufgaben im Rahmen des individuellen Könnens selbständig und erledigt sie vereinbarungsgemäß und vollständig.|||||
|Wenn wir 100 Punkte für unser Projekt erhalten, dann würde ich sie folgendermaßen auf die Teilnehmer aufteilen:|||
## DB-Modellierung Messwesen (Übungsaufgabe/Klassenarbeit)

<span class="hidden-text" title="mastodonurl">https://bildung.social/@oerinformatik/109275737309877986</span>

<span class="hidden-text" title="arcticleurl">https://oer-informatik.de/db-modellierung-messwesen</span>

> _(50 min Bearbeitungszeit): Erstellen Sie im zugelosten Team (3 Personen) schrittweise ein physisches Datenmodell und dokumentieren Sie die Phasen im Klassenarbeits-git-Repository ihrer Gruppe._

In der aktuellen welt- und klimapolitischen Lage muss der Primärenergiebedarf und die $CO_2$-Emission gesenkt werden.
Ihr Betrieb soll hierzu eine Datenbank entwerfen, in der Umwelt- und Energieflussmesswerte gespeichert werden.
Diese Daten sollen später von einer anderen Arbeitsgruppe ausgewertet werden und auf zentral sichtbaren InfoScreens öffentlich dargestellt werden, um für Energieeinsparungen zu sensibilisieren und motivieren, Einsparziele definieren zu können und schließlich den Erfolg der Maßnahme mess- und sichtbar zu machen.

### Rahmenbedingungen

* Modellieren Sie in gelosten Dreiergruppen die Struktur der geforderten Datenbank in einem konzeptuellen, einem logischen und einem physischen Datenmodell.

* Dokumentieren Sie diesen Entwurf in der Readme.MD-Datei des Klassenarbeits-git-Repository ihrer Gruppe. Nutzen Sie fachgerechte Darstellungsformen der einzelnen Modelle und stellen Sie alle bekannten Eigenschaften der Datenmodelle dar.

* Commits bis zum Ende der Bearbeitungszeit gehen in die Bewertung ein.

* Geben Sie zum Ende der Bearbeitungszeit über den Abgabelink die Bewertungsmatrix für die Teamarbeit ab.

* Die Internetnutzung und alle anderen Medien sind erlaubt, Kommunikation darf jedoch nur innerhalb ihres Teams erfolgen.

### Bewertungsmatrix

|Kriterium|erreichbare Punkte|erreichte Punkte|
|---|---|---|
|**Entity Relationship Modell**<br/>Kurze einführende Beschreibung der Planungsphase<br/>Vollständigkeit des Modells<br/>Fachliche Richtigkeit der Notation und Modellierung<br/>Abgabe als ERD, eingebunden in `Readme.MD` des Repos|25%||
|**Relationenmodell**<br/>Kurze einführende Beschreibung der Planungsphase<br/>Vollständige Transformation aller Infos aus dem ERD<br/>Fachlich Richtige Darstellung<br/>Nachweis der Normalform für jedes Relationenschema<br/>Abgabe als RM (Textform oder Diagramm), eingebunden in `Readme.MD` des Repos|25%||
|**Physisches Modell**<br/>Kurze einführende Beschreibung der Planungsphase<br/>Nennung des genutzten DBMS (sonst wird MariaDB angenommen)<br/>Korrekte Notation gemäß DDL des jeweiligen DBMS<br/>Abgabe als SQL-DDL-Befehle, eingebunden in `Readme.MD` des Repos|25%||
|**Gruppenphase** (Kriterien s.u., Abgabe auf Papier)||
|Bewertung Teamkompetenzen durch Gruppenmitglieder (siehe unten)|8,6%||
|Selbstbewertung (siehe unten)|8,6%||
|Bewertung durch Lehrer|8,7%||
|**Ergebnis**|100%||

Darüber hinaus gibt es noch einen _procored_ Wissenstest, der über Moodle im Vorfeld bearbeitet wird. Der Moodletext wird mit 40% gewichtet, die Gruppenarbeit mit 60%.

### Konzeptuelles Datenmodell (25%):


Aus einem ersten Gespräch mit der Fachdomäne erhalten Sie folgende Liste der Anforderungen:

- Es sollen verschiedene Zähler ausgelesen werden und die Messwerte der Zähler erfasst werden. Ein Messdatensatz umfasst dabei immer den Messwert selbst, Beginn und Ende der Messung, die gemessene Größe (z.B. Strombezug, Erdgasbezug, Trinkwasserbezug, Temperatur) und die Einheit (z.B. $kWh$, $m^3$, $°C$), in der der Messwert gespeichert wird.

- Jeder Messwert ist einem Zählpunkt zugeordnet. Zählpunkte sind abstrakte unveränderliche Identifikatoren, um bestimmte Messungen an Liegenschaften auch bei Zähleraustausch eindeutig zu halten.
Zählpunkte werden über eine 33-stellige Zählpunktbezeichnung identifiziert.

- Die Messung selbst erfolgt mit einem Zähler, der wiederum mit Hilfe einer Zählernummer identifiziert wird. Jedem Zählpunkt ist zu einem Zeitraum ein Zähler zugeordnet. Zähler können aber auch getauscht werden. Die Messung eines Zählpunkts wird dann von einem anderen Zähler übernommen. Es ist auch möglich, dass getauschte Zähler an einem anderen Zählpunkt wieder zum Einsatz kommen.

- Einem Zählpunkt ist genau eine Liegenschaft zugeordnet. Liegenschaften können sowohl Privatwohnungen wie auch Geschäftsadressen sein, daher soll neben den üblichen Adress-Attributen lediglich ein "Name" abgefragt werden (und nicht Vorname, Nachname oder Geschäftsform). An Liegenschaften können mehrere Zählpunkte existieren (z.B. einer für Strombezug, einer für Gasbezug, einer für Stromeinspeisung usw.).

- Jeder Liegenschaft und jedem Zähler soll genau ein Foto zugeordnet werden. Diese Fotos sollen direkt in der Datenbank abgespeichert werden, allerdings in einer eigenen Tabelle.

Erstellen Sie ein Entity-Relationship-Modell. Führen Sie hierzu nur die Planungsschritte durch, die in der Phase der konzeptuellen Datenmodellierung fachgerecht sind. Fügen Sie ein Entity-Relationship-Diagramm (in Chen-Notation) in ihr git-Repository ein (toolunterstützt oder als Stiftzeichnung als Foto/Screenshot). Kennzeichnen Sie Kardinalitäten (inkl. Optionalität) sowie optionale, mehrwertige oder abgeleitete Attribute, Relationstypen und optionale Relationen.

### Logisches Datenmodell (25%):

Das obige Datenmodell soll in ein Relationenmodell überführt werden. Führen Sie alle Transformationsschritte durch, die hierzu in der logischen Datenbankmodellierung erforderlich sind. Benennen Sie - soweit erforderlich - auch die Constraints, die sich aus den konzeptuellen Modell ergeben. Achten Sie darauf, dass im Relationenmodell mindestens der gleiche Informationsgehalt existiert, wie im ER-Modell.

Weisen Sie nach, dass ihr Relationenmodell der 3. Normalform genügt bzw. begründen Sie, an welchen Stellen Sie sich bewusst dagegen entschieden haben.

### Physisches Datenmodell (25%):

Entscheiden Sie, welches DBMS Ihnen als Grundlage dienen soll. Dauert die Entscheidung länger als zwei Minuten ist die Grundlage ihrer Lösung MariaDB. Erstellen Sie für das logische Modell die SQL-DDL-Befehle.

Fügen Sie für alle Attribute bei Datentypen und Constraints, bei denen Sie Entscheidungen getroffen haben, kurz als Kommentar einen Stichpunkt als Begründung ein.

```sql
CREATE TABLE beispiel (
    attributname1 DATENTYP CONSTRAINT /*Kurzbegründung f. Constraint od. Datentyp*/,
    attributname2 DATENTYP CONSTRAINT /*Kurzbegründung f. Constraint od. Datentyp*/,
    ...);
```


### Bewertungskriterien für die Gruppenphase (ca. 3 min Bearbeitung nach Abgabe der Repos)

Die Bewertung der Gruppenprozesse ist essenzieller Bestandteil dieser Gruppen-Klassenarbeit. Diese Bewertung erfolg aus drei Perspektiven: Jede*r bewertet sich selbst (1. Spalte) und seine beiden Gruppenmitglieder (2. und 3. Spalte). Ausserdem erfolgt durch den Lehrer eine Bewertung auf Basis der Beobachtungen während der Bearbeitung.

`++` = trifft voll zu (100%) / `+` = trifft eher zu (75%) / `-` trifft eher nicht zu (50%) / `--` = trifft nicht zu (0%)

|Kriterium|selbst|Teil-<br/>nehmer*in<br/> 1|Teil-<br/>nehmer*in<br/> 2|
|---|---|---|---|---|
|Sorgt für eine konzentrierte und zielgerichtete Arbeitsatmosphäre im Team.|||||
Arbeitet kooperativ mit den anderen Teammitgliedern: ergänzt, bestärkt, ohne Arbeit an sich zu reißen oder andere zu übergehen.|||||
|Übernimmt Aufgaben im Rahmen des individuellen Könnens selbständig und erledigt sie vereinbarungsgemäß und vollständig.|||||
|Bietet aktiv Hilfe und Unterstützung an und unterstützt bei Bedarf bzw. fordert aktiv Hilfe ein und kann sie annehmen.|||||
|Übt konstruktiv Kritik an Arbeitsweise und Ergebnissen und nimmt konstruktive Kritik an und hilft bei Umsetzung der Vorschläge. |||||
|Teilt Wissen und Informationen oder beschafft sie auch für andere.|||||

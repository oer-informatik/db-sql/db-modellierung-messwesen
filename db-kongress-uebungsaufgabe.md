## SQL-Übungsaufgaben (Erstellen einer Datenbank zur Kongress-Organisierung)

<script type="text/javascript" src="https://oer-informatik.gitlab.io/service/ci-pipeline/src/oer-scripts.js" id="oer-script-js"></script>

<span class="hidden-text" title="arcticleurl">https://oer-informatik.de/db-kongress-uebungsaufgabe</span>

<span class="hidden-text" title="mastodonurl">https://bildung.social/@oerinformatik/111744180002021967</span>

> **tl/dr;** _Übungsaufgaben zur Datenbank-Modellierung und einfachen SQL-Abfragen am Beispiel einer Datenbank für die Planung eines Kongresses. Die einzelnen Schritte werden exemplarisch (an Teilbereichen der Datenbank) abgefragt. Die Aufgaben waren Bestandteil einer klassischen Zettel/Stift Klassenarbeit. Es soll ein konzeptuelles Datenmodell erstellt werden, dieses soll in ein logisches Datenmodell transformiert werden. Das Relationenschema soll normalisiert werden, schließlich soll die Datenstruktur erstellt und die Daten einer Tabelle eingefügt, geändert, gelöscht und ausgelesen werden._

### Ausgangslage

Wir planen an unserer Schule einen Hauskongress, ein _Barcamp_. An Stelle des normalen Berufsschulunterrichts bieten Schülerinnen und Schüler der Fachinformatik-Klassen Vorträge, Tutorials, Workshops, Diskussionen an. Diese Veranstaltungen müssen irgendwie organisiert und dokumentiert werden. Mit dieser Hintergrundinfo stürzen wir uns auf die erste Teilaufgabe:

### Konzeptueller Entwurf mit dem Entity-Relationship-Diagramm

Es soll eine Datenbank entworfen werden zur Planung und Umsetzung einer selbstorganisierten Konferenz (ein „Barcamp“). Bei dem Kick-off-Meeting mit dem Planungsteam wurden folgende Anforderungen festgehalten: 

- Auf der Konferenz gibt es unterschiedliche Angebote, die sich _Sessions_ nennen.

- Sessions haben ein Thema und eine Beschreibung. 

- Sessions können unterschiedliche Formate haben (Beispiele sind Diskussionen, Präsentationen, Workshops). Jedes Format hat dabei einen Namen, eine Beschreibung und eine bestimmte Dauer.

- Genau eine Person ist verantwortlich für eine Session. Es gibt aber oft mehrere Personen, die sie gemeinsam vorbereiteten. Personen werden mit Vor- und Nachname sowie E-Mail-Adresse gespeichert.

- Da Sessions innerhalb der Konferenz mehrfach gehalten werden können, werden sie als Veranstaltungen in einem _Timetable_ organisiert: Dort wird für jede Veranstaltung eine eigene Veranstaltungsnummer festgehalten, außerdem wird dort erfasst, um welche Session es sich handelt, wann die Veranstaltung beginnt. Der Endzeitpunkt wird dort auch angezeigt, aber aus der Dauer des Sessionformats berechnet. 

- Im _Timetable_ wird für jede Veranstaltung auch der Ort erfasst, wobei hier bisher nur spezifiziert ist, dass es mehrerer Informationen sind, das genaue Format ist noch nicht gegeben (könnte `GebäudeNr`, `RaumNr`, oder `Adresse` oder `Geolocation` sein – oder alles. Das muss später erfasst werden.)

- Es gibt Personen, die keine oder mehrere Sessions vorbereiten oder für keine oder mehrere Sessions verantwortlich sind.

- Zu einer Session gehört genau ein Dokument, in dem die Informationen gespeichert werden. Das Dokument soll in einer gesonderten Tabelle erfasst werden.

Erstelle für die genannten Punkte ein _Entity-Relationship_-Diagramm in _Chen_-Notation unter Nutzung der Notationsmittel für die oben genannten Eigenschaften!

<button onclick="toggleAnswer('dbkon1')">Antwort ein/ausblenden</button>

<span class="hidden-answer" id="dbkon1">

![ER-Diagramm der geforderten Aufgabenstellung](db-kongress/barcamp.png)

Punkteverteilung ca.

- 5 Entitätstypen 3P = 15P

- 5 Relationen 1P = 5P

- abgeleitetes Attribut 1P

- mehrwertiges Attribut erkannt 2P

- Optionalität bei Person 1P


Häufige Fehler:

- An Attributen werden _keine_(!) Kardinalitäten notiert

- Chen verlangt benannte Relations-Rauten! (-2P)

- Attribute werden als Ellipsen dargestellt (-2P)

- Primär- und Fremdschlüssel sind hier noch nicht nötig
 
</span>

### Logischer Entwurf mit dem Relationenmodell (21 Punkte)

Hinweis: die folgenden Fragen betreffen Bereiche der Datenbank, die unter Umständen im ER-Diagramm der vorigen Aufgabe so nicht modelliert werden sollten.

#### Transformation ins Relationenmodell (9 Punkte)

Stelle den folgenden Sachverhalt im Relationenmodell dar. Alle Attribute sind eingabepflichtig.

![ER-Diagramm in Martin-Notation](db-kongress/er-transformation.png)

<button onclick="toggleAnswer('dbkon2')">Antwort ein/ausblenden</button>

<span class="hidden-answer" id="dbkon2">

Das Relationenmodell in Textform:

```
Timetable(VeranstaltungsID<<PK>>)
Teilnahme(VeranstaltungsID <<PK>> <<FK>>, E-Mail <<PK>> <<FK>>)
Person (E-Mail <<PK>>, Vorname, Nachname)
```

Das Relationenmodell als Diagramm:

![Relationenmodell entsprechend der obigen Angaben](db-kongress/barcampRelationenmodell.png)

</span>


#### Schlüsselattribute (4 Punkte)

Erkläre an obigem Beispiel den Unterschied zwischen einem künstlichen und einem natürlichen Primärschlüssel.

<button onclick="toggleAnswer('dbkon3')">Antwort ein/ausblenden</button>

<span class="hidden-answer" id="dbkon3">

**Natürliche Primärschlüssel** sind eingabepflichtige und einzigartige Attribute (oder Attributkombinationen) sein, die ohnehin erfasst werden und nicht extra neu angelegt werden. Mit diesen Eigenschaften dienen die Attribute dazu, eine Entität eindeutig zu identifizieren. In obigem Beispiel entspricht das Attribut _E-Mail_ diesen Anforderungen.

**Künstliche Primärschlüssel** hingegen werden als neues Attribut geschaffen, um die Entitäten eindeutig identifizieren zu können. Sie ergeben sich nicht automatisch aus der Nominalextraktion der Aufgabenstellung, sonder werden beispielsweise als laufende Nummern oder UUID zusätzlich erzeugt mit dem alleinigen Zweck, Entitäten eindeutig zu identifizieren. Die VeranstaltungsID wäre im obigen Beispiel ein künstlicher Primärenergieschlüssel. 

</span>

#### Transformation (8 Punkte)

Transformiere die Beziehungen und Entitätstypen des folgende ER-Modells auf zwei unterschiedlichen Wegen in ein Relationenmodell. Für jede der beiden Lösungen nenne bitte Randbedingungen, unter denen Du dich für diese Variante entscheiden würdest.


<button onclick="toggleAnswer('dbkon4')">Antwort ein/ausblenden</button>

<span class="hidden-answer" id="dbkon4">

_Eine_ mögliche Beispiellösung (viele andere möglich):

- Variante 1: beide Entitätstypen in einer Relation mit einer ID; (Diese Option ist sinnvoll, wenn kein großer Speicherbedarf gefragt ist und beide Werte i.d.R. häufig gemeinsam vorkommen).

- Variante 2: Beide Relationen erhalten den gleichen Primärschlüssel; (Diese Option ist sinnvoll,wenn eine der beiden Entitätstypen optional ist und die andere immer vorkommt).

</span>

### Normalisierung	(21 P)

Für das Barcamp wurde ein Formular erstellt, mit dem um Beteiligung gebeten wurde ("Call for Participation"). Die Formularergebnisse liegen als Tabelle vor, die nun normalisiert werden soll, um der dritten Normalform zu genügen.

|Sessionthema|Beschreibung|Format|Organisatoren|Verantwortliche/r|E-Mail|Klasse|
|---|---|---|---|---|---|---|
|Normalisierung|Es geht um …|Talk|Murat, Eva|Eva|eva@a.bc|IFA4D|
|DB-Entwurf|Ich werde…|Panel|Kim, Ayse, Jan|Jan|jan@ab.c|IFA5A|
|DBMS mit Docker|Grundlagen…|Workshop|Anna, Ali, Sybel|Sybel|sybel@ab.cd|IFA4D|

a) Welche Anpassungen sind nötig, damit das obige Relationenschema der 1. Normalform genügt? (3 Punkte)

<button onclick="toggleAnswer('dbkon5')">Antwort ein/ausblenden</button>

<span class="hidden-answer" id="dbkon5">

Das Attribut „Organisatoren“ ist nicht atomar, sondern eine List und muss aufgeteilt werden. Es wird für jeden Listeneintrag eine eigene Zeile erstellt.

</span>


b) Benennen Sie den Unterschied zwischen der zweiten und der dritten Normalform von Relationenmodellen!	(8 Punkte)

<button onclick="toggleAnswer('dbkon6')">Antwort ein/ausblenden</button>

<span class="hidden-answer" id="dbkon6">

In der zweiten Normalform wird geprüft, ob es Abhängigkeiten von Nichtschlüsselattributen und Teilschlüsseln gibt, während in der dritten Normalform die Abhängigkeiten von Nichtschlüsselattributen untereinander untersucht werden.

</span>



c) Erstellen Sie für die oben dargestellten Formulardaten das Relationenmodell in der 3. Normalform (es müssen nur die Attributnamen genannt werden, nicht die Werte!) 	(10 Punkte)


<button onclick="toggleAnswer('dbkon7')">Antwort ein/ausblenden</button>

<span class="hidden-answer" id="dbkon7">

Session(SessionID (PK), Thema, Beschreibung, Format, Name (FK)) (4P)
Person(Name (PK), E-Mail, Klasse) (3P)
Organisatoren(SessionID (PK, FK), Name (PK, FK)) (3P)

(Wenn Name PK ist)

![Relationenmodell mit dem PK E-Mail](db-kongress/db-kon-normalisiert.png)

</span>

### Physisches Modell / SQL-DDL	(18 P)

#### Tabellenerzeugung

Erstelle alle vollständigen DDL-Befehle in korrekter Reihenfolge, um die beiden folgenden Tabellen in eine bestehende Datenbank Barcamp zu erzeugen: 	(10 Punkte)

`Timetable`

|VeranstaltungsID(PK)|RaumNr|Gebäude|SessionNr|Startzeitpunkt|
|---|---|---|---|---|
|16|R156|M2|123|10:00|

(Hinweis: SessionNr ist Primärschlüssel der Tabelle Session, VeranstaltungsID soll automatisch vergeben werden, Startzeitpunkt ist nicht eingabepflichtig)

`Räume`

|RaumNr(PK)|Gebäude(PK)|Bezeichnung|
|---|---|---|
|R156|M2|Linux-Labor|

(Hinweis: Bezeichnung ist eingabepflichtig und muss einzigartig sein)

<button onclick="toggleAnswer('dbkon8')">Antwort ein/ausblenden</button>

<span class="hidden-answer" id="dbkon8">

Zuerst:

```sql
CREATE TABLE Räume(
  RaumNr CHAR(8) PRIMARY KEY, 
  Gebäude CHAR(8), 
  Bezeichnung CHAR(32) NOT NULL UNIQUE,
  CONSTRAINT pk PRIMARY KEY (RaumNr, Gebäude)
);
```

Prinzipieller Aufbau 1P
Datentypen 1P
PK 1P
UNIQUE NOT NULL 2P

Danach:

```sql
CREATE TABLE Timetable (
  VeranstaltungsID AUTO_INCREMENT PRIMARY KEY, 
  RaumNr CHAR(8) FOREIGN_KEY REFERENCES Räume(RaumNr), 
  Gebäude CHAR(8) FOREIGN_KEY REFERENCES Räume(Geebäude),, 
  SessionNr INTEGER FOREIGN_KEY REFERENCES Session(SessionNr), 
  Startzeitpunkt TIME)
);
```

Prinzipieller Aufbau 1P
Datentypen 1P
PK 1P
3x FK 2P

</span>


#### Änderung der Tabellenstruktur

Es soll in der bestehenden Tabelle _Timetable_ ein Feld Beschreibung (als Zeichenkette) ergänzt werden. Benennen Sie den nötigen SQL-Befehl! 	(___ von 4 Punkten)

<button onclick="toggleAnswer('dbkon9')">Antwort ein/ausblenden</button>

<span class="hidden-answer" id="dbkon9">

```
ALTER TABLE Timetable ADD Beschreibung CHAR(32)
```

</span>


#### Datentypen

Die meisten Datenbankmanagement-Systeme bieten zwei unterschiedliche Datentypen für Zeichenketten an: `VARCHAR` und `CHAR`. Erläutere, was die beiden Datentypen unterscheidet, und folgere daraus, für welche Anwendungsfälle sie geeignet sind. (4 Punkte)

<button onclick="toggleAnswer('dbkon10')">Antwort ein/ausblenden</button>

<span class="hidden-answer" id="dbkon10">

`CHAR` reserviert mehr Speicher, ist dafür aber performanter und die beste Wahl, wenn es um Geschwindigkeit geht.
`VARCHAR` belegt nur so viel Speicher, wie es benötigt und ist daher die beste Wahl, wenn Speicherplatz gespart werden muss. 

</span>


### SQL-DML	(14 P)

a) Füge die beiden Zeilen aus den Beispielen der vorigen Aufgabe (Raum156, VeranstaltungNr. 16) in die soeben erstellten Tabellen ein. Gebe alle dafür nötigen SQL-Befehle in der korrekten Reihenfolge an! Es kann vorausgesetzt werden, dass die Session Nr. 123 bereits existiert. 	(8 Punkte)


<button onclick="toggleAnswer('dbkon11')">Antwort ein/ausblenden</button>

<span class="hidden-answer" id="dbkon11">

```sql
INSERT INTO Räume (RaumNr, Gebäude, Bezeichnung) VALUES (‘R156‘, ‘M2‘, ‘Linux-Labor‘); //4P
INSERT INTO Timetable (VeranstaltungsID, RaumNr, Gebäude, SessionNr, Startzeitpunkt) VALUES(NULL, ‘R156‘, ‘M2‘, ‘123‘, ‘10:00‘); );//4P
```

</span>

b) Die Veranstaltung 16 wird in Raum R555 verlegt. Benenne alle erforderlichen SQL-Befehle, wenn der Raum bereits existiert! 	(3 Punkte)


<button onclick="toggleAnswer('dbkon12')">Antwort ein/ausblenden</button>

<span class="hidden-answer" id="dbkon12">

```sql
UPDATE Timetable SET RaumNr = 'R555' WHERE VeranstaltungsID = '16';
```

</span>

c) Es gab eine Fehleingabe, die nun rückgängig gemacht werden muss: Benennen Sie alle SQL-Befehle, die erforderlich sind, um die Veranstaltung 19 aus der Datenbank zu entfernen! 	(3 Punkte)

`Timetable`

|VeranstaltungsID(PK)|RaumNr|Gebäude|SessionNr|Startzeitpunkt|Beschreibung|
|---|---|---|---|---|---|
|19|R556|M2|113|12:00|geplant|

<button onclick="toggleAnswer('dbkon13')">Antwort ein/ausblenden</button>

<span class="hidden-answer" id="dbkon13">

```sql
DELETE FROM Timetable WHERE VeranstaltungsID = 19;
```

</span>

### SQL-DQL	(8 P)

Die Tabelle `Timetable`` hat folgenden Aufbau und Beispielinhalt:

|VeranstaltungsID(PK)|RaumNr|Gebäude|SessionNr|Startzeitpunkt|Beschreibung|
|---|---|---|---|---|---|
|16|R156|M2|123|10:00|Ok, fällt aus|
|17|R155|M2|126|11:00|fällt aus, leider|
|18|R157|M2|126|10:30|ok|
|19|R556|M2|112|12:00|geplant|

a) Es sollen alle Veranstaltungen ausgegeben werden, bei denen in der Beschreibung steht, dass Sie ausfallen. Benennen Sie den nötigen SQL-Befehl anhand der obigen Beispieldaten! 	(4 Punkte)

<button onclick="toggleAnswer('dbkon14')">Antwort ein/ausblenden</button>

<span class="hidden-answer" id="dbkon14">

```sql
SELECT * FROM Timetable WHERE Beschreibung LIKE "%fällt%"
```

</span>

b) Es sollen eine Liste aller VeranstaltungsID und SessionNr ausgegeben werden, die in R156 stattfinden. Benennen Sie den nötigen SQL-Befehl anhand der obigen Beispieldaten!	(4 Punkte)

<button onclick="toggleAnswer('dbkon15')">Antwort ein/ausblenden</button>

<span class="hidden-answer" id="dbkon15">

```sql
SELECT VeranstaltungsID, SessionNr FROM Timetable WHERE RaumNr = 'R156' 
```

</span>

